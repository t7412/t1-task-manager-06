package ru.t1.chubarov.tm;

import ru.t1.chubarov.tm.constant.ArgumentConst;
import ru.t1.chubarov.tm.constant.CommandConst;

import java.util.Scanner;

import static ru.t1.chubarov.tm.constant.CommandConst.*;


public class Application {

    public static void main(final String[] args) {
        processArguments(args);
        System.out.println("** WELCOME TASK MANAGER **");
        final Scanner scanner = new Scanner(System.in);
        while (!Thread.currentThread().isInterrupted()){
            System.out.println("ËNTER COMMAND:");
            final String command = scanner.nextLine();
            processACommand(command);
        }
    }

    private static void processArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        processArgument(args[0]);
        System.exit(0);
    }

    private static void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            default:
                showArgumentError();
                break;
        }
    }

    private static void processACommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case CommandConst.ABOUT:
                showAbout();
                break;
            case CommandConst.VERSION:
                showVersion();
                break;
            case CommandConst.HELP:
                showHelp();
                break;
            case CommandConst.EXIT:
                exit();
                break;
            default:
                showCommandError();
                break;
        }
    }

    private static void exit(){
        System.exit(0);
    }
    private static void showArgumentError() {
        System.out.println("[ERROR]");
        System.out.println("This argument not supported");
        System.exit(1);
    }

    private static void showCommandError() {
        System.out.println("[ERROR]");
        System.out.println("This command not supported");
        System.exit(1);
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Evgeni Chubarov");
        System.out.println("e-mail: echubarov@t1-consulting.ru");
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.6.0");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s, %s : Show about program. \n", CommandConst.ABOUT, ArgumentConst.ABOUT);
        System.out.printf("%s, %s : Show program version \n", CommandConst.VERSION, ArgumentConst.VERSION);
        System.out.printf("%s, %s : Show list arguments \n", CommandConst.HELP, ArgumentConst.HELP);
        System.out.printf("%s - Close appliction \n", EXIT);
    }

}
